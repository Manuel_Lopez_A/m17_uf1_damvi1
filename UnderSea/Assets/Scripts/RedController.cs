using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedController : MonoBehaviour
{

    public int spd;

    // para juntarlo con el UI - DELEGADOS
    // delegados es una variable que tiene dentro una funci�n
    public delegate void Punto(int p);
    public event Punto OnPunto;  // esto es un evento


    void Start()
    {
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(spd, 0);
    }

    // Update is called once per frame
    void Update()
    {
        if (this.GetComponent<Rigidbody2D>().transform.position.x > 11)
        {
            Destroy(this.gameObject);
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        print("red ha cazado " + collision.name);

        if (collision.tag == "pulpo")
        {
            // if (OnPunto != null) { OnPunto.Invoke() };
            OnPunto?.Invoke(10); // es lo mismo que decir  lo que pone en la linea superior

            Destroy(collision.gameObject);
            Destroy(this.gameObject);
        }
        if (collision.tag == "pez")
        {
            // if (OnPunto != null) { OnPunto.Invoke() };
            OnPunto?.Invoke(2); // es lo mismo que decir  lo que pone en la linea superior

            Destroy(collision.gameObject);
            Destroy(this.gameObject);
        }
        if (collision.tag == "tiburon")
        {
            collision.GetComponent<TiburonController>().spd *= 2;
            collision.GetComponent<TiburonController>().enfado();
            collision.GetComponent<SpriteRenderer>().color = Color.red;
        }

    }
}
