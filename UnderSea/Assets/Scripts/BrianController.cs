using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrianController : MonoBehaviour
{
    public int spd;

    // Start is called before the first frame update
    void Start()
    {
        this.GetComponent<Rigidbody2D>().velocity = new Vector3(-spd, 0);
    }

    // Update is called once per frame
    void Update()
    {
        if (this.GetComponent<Rigidbody2D>().position.x < -10)
        {
            Destroy(this.gameObject);
        }
    }
    public void enfado()
    {
        this.GetComponent<Rigidbody2D>().velocity = new Vector3(-spd, 0);
        this.GetComponent<AudioSource>().Play();
    }
}
