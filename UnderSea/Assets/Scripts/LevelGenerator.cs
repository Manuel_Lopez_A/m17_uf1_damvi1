using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGenerator : MonoBehaviour
{

    public GameObject[] groundPrefab;
    public float tiempoEspera;

    void Start()
    {

        StartCoroutine(crearSuelo());
        
        //primera vez que se crea un suelo
        GameObject newSuelo = Instantiate(groundPrefab[0]);
        newSuelo.transform.position = new Vector3(-1.8f, -3.8f, 0);
    }

    // Update is called once per frame
    void Update()
    {
        if (this.transform.position.x < -10)
        {
            Destroy(this.gameObject);
        }
    }

    IEnumerator crearSuelo()
    {
        while (true)
        {
            int sueloCreado = Random.Range(0, 3);
            GameObject newSuelo = Instantiate(groundPrefab[sueloCreado]);
            newSuelo.transform.position = new Vector3(this.transform.position.x, Random.Range(-4f, 0), 0);

            // esto es lo que esperas
            tiempoEspera = Random.Range(2, 4);
            if (sueloCreado == 0)
            {
                tiempoEspera *= 2;
            }
            else if (sueloCreado == 1)
            {
                tiempoEspera*=1.2f;
            }
            yield return new WaitForSeconds(tiempoEspera);
        }
    }
}
