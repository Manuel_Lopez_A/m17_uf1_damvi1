using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;

public class JugadorController : MonoBehaviour
{
    public int spd;
    public GameObject disparo;
    public float tiempoBala;
    private bool condemor = false;
    // hacer referencia a la User Interface
    public UIManager uiManager;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.A))
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2 (-spd, this.GetComponent<Rigidbody2D>().velocity.y);
            // this.GetComponent<Rigidbody2D>().velocitynew Vector2(spd, this.GetComponent<Rigidbody2D>().velocity));
        }
        else if (Input.GetKey(KeyCode.D))
            
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(spd, this.GetComponent<Rigidbody2D>().velocity.y);
        }
        else
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, this.GetComponent<Rigidbody2D>().velocity.y);

        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(spd, 500));
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            if (!condemor)
            {
                GameObject bang = Instantiate(disparo);
                bang.transform.position = this.transform.position;
                bang.GetComponent<balaController>().OnPunto += uiManager.Puntua;
                condemor = true;
                StartCoroutine(Condemor());
            }

        }

    }

    IEnumerator Condemor()
    {
        // forma de controlar el tiempo
        yield return new WaitForSeconds(tiempoBala);
        condemor = false;

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        print("jugador ha chocado contra" + collision.name);
        if(collision.tag == "MALO")
        {
            //    Destroy(this.gameObject);
            // un cambio de escena destruye todo lo creado
            SceneManager.LoadScene("GameOver");
        }

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        //print("jugador ha chocado con " + collision.name);
        if(collision.transform.tag == "MALISIMO")
        {
            print("Jugador ha chocado con BRIAN");
            //     Destroy(this.gameObject);
            // un cambio de escena destruye todo lo creado
            SceneManager.LoadScene("GameOver");
        }
    }



}
