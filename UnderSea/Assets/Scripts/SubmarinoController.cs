using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SubmarinoController : MonoBehaviour
{
    public int vidas = 3;
    public int spd;
    public GameObject disparo;
    public GameObject red;
    public float tiempoMisil;
    public float tiempoRed;
    private bool condemorDisparo = false;
    private bool condemorRed = false;
    // hacer referencia a la User Interface
    public UIManager uiManager;



    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
        if (Input.GetKey(KeyCode.DownArrow) && this.GetComponent<Rigidbody2D>().transform.position.y > -3.4f )        //BAJA
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, -spd);
        }
        else if (Input.GetKey(KeyCode.UpArrow) && this.GetComponent<Rigidbody2D>().transform.position.y < 4f)   //SUBE
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, spd);
        }
        else                                //QUIETO
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, 0);
        }
        if (Input.GetKeyDown(KeyCode.Space))//LANZA RED
        {
            //

            if (!condemorRed)
            {
                GameObject redPesca = Instantiate(red);
                redPesca.transform.position = this.transform.position;
                redPesca.GetComponent<RedController>().OnPunto += uiManager.Puntua;
                condemorRed = true;
                StartCoroutine(CondemorRed());
            }
        }

        //disparar con temporizador --
        //----------------------------

        if (Input.GetKeyDown(KeyCode.C))
        {
            if (!condemorDisparo)
            {
                GameObject bang = Instantiate(disparo);
                bang.transform.position = this.transform.position;
                bang.GetComponent<MisilController>().OnPunto += uiManager.Puntua;
                condemorDisparo = true;
                StartCoroutine(CondemorDisparo());
            }

        }
    }

    IEnumerator CondemorDisparo()
    {
        // forma de controlar el tiempo
        yield return new WaitForSeconds(tiempoMisil);
        condemorDisparo = false;

    }
    IEnumerator CondemorRed()
    {
        // forma de controlar el tiempo
        yield return new WaitForSeconds(tiempoRed);
        condemorRed = false;

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        print("submarino ha chocado contra" + collision.name);
        if (collision.tag == "tiburon")
        {
            print("Jugador ha chocado con : " + collision.gameObject.name);
           
                vidas -- ;
                uiManager.Vidas();
                if(vidas == 0)
                {
             
                SceneManager.LoadScene("GameOver02");
            }
                // poner rojo ...
          
        } else if (collision.tag == "mina")
        {
            print("has chocado con una mina");
            vidas = 0;
            SceneManager.LoadScene("GameOver01");
        }

    }

}
