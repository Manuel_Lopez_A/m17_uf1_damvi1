using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MisilController : MonoBehaviour
{

    // velocidad dl misil, sale en la interfaz gr�fica del programa
    public int spd;

    // para juntarlo con el UI - DELEGADOS
    // delegados es una variable que tiene dentro una funci�n
    public delegate void Punto(int p);
    public event Punto OnPunto;  // esto es un evento


    void Start()
    {
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(spd, 0);
    }

    // Update is called once per frame
    void Update()
    {
        if (this.GetComponent<Rigidbody2D>().transform.position.x > 11)
        {
            Destroy(this.gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        print("misil ha chocado contra" + collision.name);

        if (collision.tag == "tiburon")
        {
            // if (OnPunto != null) { OnPunto.Invoke() };
            OnPunto?.Invoke(10); // es lo mismo que decir  lo que pone en la linea superior

            Destroy(collision.gameObject);
            Destroy(this.gameObject);
        }
        if (collision.tag == "pulpo")
        {
            collision.GetComponent<BrianController>().spd *= 2;
            collision.GetComponent<BrianController>().enfado();
            collision.GetComponent<SpriteRenderer>().color = Color.red;
        }
    }
}
