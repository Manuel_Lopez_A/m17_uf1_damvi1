using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TiburonController : MonoBehaviour
{
    public int spd;
    private bool vidas = true;
    // Start is called before the first frame update
    void Start()
    {
        spd = 6;
        this.GetComponent<Rigidbody2D>().velocity = new Vector3(-spd, 0);
    }

    // Update is called once per frame
    void Update()
    {
        if (this.GetComponent<Rigidbody2D>().position.x < -10)
        {
            Destroy(this.gameObject);
        }

    }

    public void enfado()
    {
        if(vidas)
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector3(-spd, 0);
            //this.GetComponent<AudioSource>().Play();
            vidas = false;
        }else
        {
            Destroy(this.gameObject);
        }
    }


}
