using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{

    public GameObject[] peces;
    public int tiempoEsperaMin;
    public int tiempoEsperaMax;
    public int maxPeces;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(crearPez());
        maxPeces = peces.Length;
    }

    // Update is called once per frame
    void Update()
    {
        if (this.transform.position.x < -10)
        {
            Destroy(this.gameObject);
        }
    }
    IEnumerator crearPez()
    {
        while (true)
        {
            GameObject newPez = Instantiate(peces[Random.Range(0,maxPeces)]);
            // Instantiate(new);
            // GameObject newStewie = Instantiate(stewie);
            newPez.transform.position = new Vector3(this.transform.position.x, Random.Range(-2f, 4f), 0);

            // esto es lo que esperas
           int tiempoEspera = Random.Range(tiempoEsperaMin, tiempoEsperaMax);
           yield return new WaitForSeconds(tiempoEspera);
        }
    }
}
