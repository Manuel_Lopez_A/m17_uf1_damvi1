using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class balaController : MonoBehaviour
{   
    // velocidad de la bala, sale en la interfaz gr�fica del programa
    public int spd;


    // para juntarlo con el UI - DELEGADOS
    // delegados es una variable que tiene dentro una funci�n
    public delegate void Punto(int p);
    public event Punto OnPunto;  // esto es un evento
    // public event Punto OnPuntoDos;


    // Start is called before the first frame update
    void Start()
    {
        this.GetComponent<Rigidbody2D>().velocity = new Vector2 (spd, 0);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        print("bala ha chocado contra" + collision.name);
        if (collision.tag == "MALO")
        {
            // if (OnPunto != null) { OnPunto.Invoke() };
            OnPunto?.Invoke(10); // es lo mismo que decir  lo que pone en la linea superior

            Destroy(collision.gameObject);
            Destroy(this.gameObject);
        }
        if (collision.tag == "MALISIMO")
        {
            collision.GetComponent<BrianController>().spd *= 2;
            collision.GetComponent<BrianController>().enfado();
            collision.GetComponent<SpriteRenderer>().color = Color.red;
        }
    }
}
