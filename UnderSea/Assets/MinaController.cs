using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MinaController : MonoBehaviour
{
    public int spd;
    // Start is called before the first frame update
    void Start()
    {
        spd = 3;
        this.GetComponent<Rigidbody2D>().velocity = new Vector3(-spd, 0);
    }

    // Update is called once per frame
    void Update()
    {
        if (this.GetComponent<Rigidbody2D>().position.x < -10)
        {
            Destroy(this.gameObject);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "Player")
        {
            print("MINA ha chocado con : " + collision.gameObject.name);

                Destroy(this.gameObject);
                            
        }

    }
}
