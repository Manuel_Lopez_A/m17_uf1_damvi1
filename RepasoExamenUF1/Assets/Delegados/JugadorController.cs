using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JugadorController : MonoBehaviour
{
    public int spd;
    public GameObject bala;
    public float tiempoDisparo;
    private bool cargador = true;
    


    // genero un delegado y luego creo un evento
    public delegate void CogerFoto();
    public event CogerFoto cogerFotoEvent;


 
    void Start()
    {
        spd = 3;
       
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.A))
        {
            // IZQUIERDA
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(-spd, this.GetComponent<Rigidbody2D>().velocity.y );
        }
        else if (Input.GetKey(KeyCode.D))
        {
            // DERECHA
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(spd, this.GetComponent<Rigidbody2D>().velocity.y);
        } 
        else
        {
            // PARA
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, this.GetComponent<Rigidbody2D>().velocity.y);
        }

        // SALTO
        if (Input.GetKeyDown(KeyCode.Space))
        {
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 500));
        }

        // DISPARO
        if (Input.GetKeyDown(KeyCode.S))
        {
              if (cargador)
            {
                Panyum();
            }
        }

    }

    private void Panyum()
    {
        GameObject bang = Instantiate(bala);

        //LA INSTANCIAMOS EN EL MISMO SITIO
        //bang.transform.position = this.transform.position;

        //LA POSICI�N LA AVANZAMOS UN POCO
        bang.transform.position = new Vector3(this.transform.position.x + 1f, this.transform.position.y, 0);

        cargador = false;
        StartCoroutine(DisparosTime());
    }


    IEnumerator DisparosTime()
    {
        // CONTROLAMOS EL TIEMPO HASTA QUE SE PONE EL BOOLEAN A FALSE.
        yield return new WaitForSeconds(tiempoDisparo);
        cargador = true;

    }

    //CUANDO COJAMOS LA LLAVE, INVOCAREMOS TODOS LOS M�TODOS QUE SE HAN SUSCRITO AL EVENTO....
    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == "Llave") {
            Destroy(collider.gameObject);
            cogerFotoEvent.Invoke(); //ojo el nombre de la funci�n/variable va con min�scula
        }
    }

    // SI CHOCAMOS CON UN ENEMIGO
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "Malo")
        {
            
            Destroy(collision.gameObject);
            //llamamos a la funci�n de uiManager para actualizar el texto
            //(esta funci�n no est� suscrita al evento...)
            print("al menos 01...");
           
        }
    }
}
