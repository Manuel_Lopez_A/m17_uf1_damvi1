using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerEnemigos : MonoBehaviour
{
    public GameObject enemigo;

    void Start()
    {
        StartCoroutine(GenerarEnemigos());
    }

    IEnumerator GenerarEnemigos()
    {
        while (true)
        {
            print("nuevo enemigo");
            Instantiate(enemigo);

            float aleatorio = UnityEngine.Random.Range(-1.5f,3.5f);
            
            enemigo.transform.position = new Vector3(this.transform.position.x , this.transform.position.y + aleatorio, 0);


            yield return new WaitForSeconds(2.0f);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
