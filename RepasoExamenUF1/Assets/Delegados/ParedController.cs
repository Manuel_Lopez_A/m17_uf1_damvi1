using System.Collections;
using System.Collections.Generic;
using UnityEditor.Experimental.GraphView;
using UnityEngine;

public class ParedController : MonoBehaviour
{
    public JugadorController jugadorC;

    void Start()
    {
        //suscribimos al evento nuestra funci�n de abrir puerta
        jugadorC.cogerFotoEvent += AbrirPuerta;
    }
    public void AbrirPuerta()
    {
        if (!this.GetComponent<BoxCollider2D>().isTrigger)
        {
            this.GetComponent<BoxCollider2D>().isTrigger = true;
            this.GetComponent<SpriteRenderer>().color = Color.green;
        } else
        {
            this.GetComponent<BoxCollider2D>().isTrigger = false;
            this.GetComponent<SpriteRenderer>().color = Color.red;
        }
    }

    //private void OnDisable()
    //{
    //    prota.OnAgafarClau -= despetjar;
    //}

}
