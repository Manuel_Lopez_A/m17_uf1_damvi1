using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BalaController : MonoBehaviour
{

    public int spd = 3;

    // para juntarlo con el UI - DELEGADOS
    // delegados es una variable que tiene dentro una funci�n
    // public delegate void Punto(int p);
    // public event Punto OnPunto;  // esto es un evento


    void Start()
    {
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(spd, 0);

    }


    void Update()
    {
        // -13 o 13 x, o -6 o 6
        // SI SALE DE LA PANTALLA LO ELIMINAS
        if (this.GetComponent<Rigidbody2D>().transform.position.x > 11)
        {
            Destroy(this.gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        print("bala ha impactado contra " + collision.name);

        if (collision.tag == "Malo")
        {
            //if (OnPunto != null)
            //{
            //    OnPunto.Invoke(10);
            //    print("invovo OnPunto");
            //}
            Destroy(collision.gameObject);
            Destroy(this.gameObject);
            
        }

    }
}
