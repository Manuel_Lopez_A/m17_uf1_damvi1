using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoController : MonoBehaviour
{
    public int spd = 3;


    void Start()
    {
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(-spd, 0);
    }

    // Update is called once per frame
    void Update()
    {
        if (this.GetComponent<Transform>().position.x < -14)
        {
            Destroy(this.gameObject);
        }
    }
}
