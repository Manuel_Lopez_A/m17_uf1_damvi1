using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CuentaAtrasControler : MonoBehaviour
{
    private int segundos = 60;
    public Button botonFinal;

    public GameDataSO datos;


    // me lo pongo en Awake para que empiece desactivado antes que empiece la corrutina
    void Awake()
    {
        botonFinal.gameObject.SetActive(false);
    }

    void Start()
    {
        //botonFinal.gameObject.SetActive(false);
        datos.tiempoRestante = segundos;
        StartCoroutine(CuentaAtras());
    }

    private IEnumerator CuentaAtras()
    {
        while (segundos > 0)
        {

            this.GetComponent<TMPro.TextMeshProUGUI>().text = segundos.ToString();
            segundos--;
            datos.tiempoRestante = segundos;
            yield return new WaitForSeconds(1f);
        }
        botonFinal.gameObject.SetActive(true);
    }
}
