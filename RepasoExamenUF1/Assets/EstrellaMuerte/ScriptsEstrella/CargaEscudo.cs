using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CargaEscudo : MonoBehaviour{

    public GameObject cargaEscudo;
    public float velocidadCargaEscudo = 2f;
    public float esperaCargaEscudo = 0.5f;

    void Start()
    {
        StartCoroutine(GenerarCarga());
    }


    private IEnumerator GenerarCarga()
    {
        while (true)
        {
            cargaEscudo.GetComponent<Rigidbody2D>().velocity = cargaEscudo.transform.right * velocidadCargaEscudo;

            if (cargaEscudo.transform.position.x > 8.5)
            {
                Destroy(cargaEscudo);
                
                SceneManager.LoadScene("MainEstrella");
                print("cargando Escena A");
            }
            yield return new WaitForSeconds(esperaCargaEscudo);
        }
        
    }
}
