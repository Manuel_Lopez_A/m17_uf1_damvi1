using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VidasControler : MonoBehaviour
{
    public GameObject escudo;
    public GameDataSO datos;

    private void Start()
    {
        //this.GetComponent<TMPro.TextMeshProUGUI>().text = "Vidas: " + datos.vidasEscudo + "/7";
        this.GetComponent<TMPro.TextMeshProUGUI>().text = "Vidas: 7/7";
        escudo.GetComponent<EscudoControler>().onDestruccion += actualizarVidas;
    }

    private void actualizarVidas()
    {
        this.GetComponent<TMPro.TextMeshProUGUI>().text = "Vidas: "+ escudo.GetComponent<EscudoControler>().vidaEscudo + "/7";
    }
}
