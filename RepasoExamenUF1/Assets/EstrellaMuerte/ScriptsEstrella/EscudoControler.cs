using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EscudoControler : MonoBehaviour
{
    public int vidaEscudo = 7;
    // le enlazamos un ScriptableObject
    public GameDataSO datos;

    // delegado del observado
    public delegate void EscudoControlerDelegate();
    public event EscudoControlerDelegate onDestruccion;

    public void Start()
    {
        // accedemos a los datos de vidasSO y le decimos que las vidas que tenemos en el escudo se pasen ah�
        datos.vidasEscudo = vidaEscudo;
    }
    public void DanyarEscudo()
    {
        vidaEscudo--;
        datos.vidasEscudo = vidaEscudo; // tendremos que actualizarlo as� en el scriptable Object?
        // cada vez que perdemos una vida llamamos al evento, todo lo suscrito har� lo que tenga que hacer
        onDestruccion.Invoke();
        SceneManager.LoadScene("EscenaB");

        if (vidaEscudo == 0)
        {
            SceneManager.LoadScene("EscenaB");
        }
    }

}
