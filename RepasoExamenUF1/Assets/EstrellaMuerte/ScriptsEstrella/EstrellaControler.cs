using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EstrellaControler : MonoBehaviour
{
    public float spd = 0.3f;

    void Update()
    {
        if (Input.GetKey(KeyCode.A))
        {
            // GIRAR IZQUIERDA
            //this.transform.Rotate(new Vector3(0, 0, spd)); // es lo mismo
            this.transform.Rotate(0,0, spd);
           
        }
        else if (Input.GetKey(KeyCode.D))
        {
            // GIRAR DERECHA
            // this.transform.Rotate(new Vector3(0, 0, -spd)); // es lo mismo
            this.transform.Rotate(0, 0, -spd);
        }

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "Rebelde")
        {
            Destroy(this.gameObject);
            SceneManager.LoadScene("GameOverEstrella");
        }
    }

}
