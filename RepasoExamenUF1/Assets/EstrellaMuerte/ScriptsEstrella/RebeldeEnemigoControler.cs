using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class RebeldeEnemigoControler : MonoBehaviour
{
    public float spd = 2f;
    public Transform posEstrella;



    
    void Update()
    {
        // creamos el vector3 en el que le damos la posicion de Estrella y le sumamos nuestra posicion
        Vector3 direccion = posEstrella.position - this.transform.position;

        // la parte de arriba de nuestra nave ser� siempre igual a la direcci�n donde est� la Estrella
        this.transform.up = direccion;

        // normalizamos la velocidad y la multiplicamos por la velocidad de la nave
        this.GetComponent<Rigidbody2D>().velocity = direccion.normalized * spd;
    }

    //El escudo es un Trigger,no tiene rigidBody
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Escudo")
        {

            print("nave enemiga colisiona con " + collision.gameObject.name);
            // llama la nave a la funci�n del escudo
            collision.GetComponent<EscudoControler>().DanyarEscudo();
            // destruye la nave
            Destroy(this.gameObject);

        }
    }
}
