using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EscenaBPersistencia : MonoBehaviour
{

    public GameDataSO datosPersistentes;

    void Start()
    {
        this.GetComponent<TMPro.TextMeshProUGUI>().text = "Vida escudo: " + datosPersistentes.vidasEscudo + "/7"+"\n"+"Tiempo restante: "+ datosPersistentes.tiempoRestante;
    }


}
