using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RebeldeSpawner : MonoBehaviour
{
    public GameObject NavesRebeldes;
    public float tiempoEspera;
    // Start is called before the first frame update
    void Start()
    {
        tiempoEspera = 2f;
        StartCoroutine(CrearNaves());
    }

    // Update is called once per frame
    void Update()
    {

    }
    public IEnumerator CrearNaves()
    {
        while (true)
        {
            int puntoPartida = Random.Range(0, 4);
            float x = 0;
            float y = 0;
            
            switch (puntoPartida)
            {
                case 0:
                    print("arriba");
                    x = Random.Range(-11, 12);
                    y = 7;
                    break;
                case 1:
                    print("derecha");
                    x = 11;
                    y = Random.Range(-7, 8);
                    break;
                case 2:
                    print("abajo");
                    x = Random.Range(-11, 12);
                    y = -7;
                    break;
                case 3:
                    print("izquierda");
                    x = -11;
                    y = Random.Range(-7, 8);
                    break;
            }

            GameObject nave = Instantiate(NavesRebeldes);
            nave.transform.position = new Vector3(x, y, 0);




            yield return new WaitForSeconds(tiempoEspera);
        }

    }
}
