using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Caca : MonoBehaviour
{

    public SOCaca so;


    public void ActivarCaca()
    {
        this.GetComponent<Rigidbody2D>().velocity = this.transform.right * so.vel;
        this.GetComponent<SpriteRenderer>().sprite = so.img;
        this.GetComponent<SpriteRenderer>().color = so.color; 

    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            print("triggerENter");
            print("mi hp es: " + this.so.hp);
        } else if (collision.tag == "Bala")
        {
            Destroy(this.gameObject);
        }
    }

    //private void OnCollisionEnter2D(Collision2D collision)
    //{
    //    if (collision.transform.tag == "Player")
    //    {
    //        print("collisionEnter");
    //        print("mi hp es: " + this.so.hp);
    //    }
    //}
}
