using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class SOCaca : ScriptableObject
{
    public int atk;
    public float vel;
    public int hp;
    public Sprite img;
    public Color color;
}
