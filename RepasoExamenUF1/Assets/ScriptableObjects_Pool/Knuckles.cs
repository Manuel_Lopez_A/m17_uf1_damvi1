using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Knuckles : MonoBehaviour
{
    public float vel = 3f;
    public GameObject bala; // s�lo sirve para el cargador normal
    public int vidas = 3;
    public float jumpForce = 500f;

    //private List<GameObject> balasPool = new List<GameObject>();
    //private int totalBalas = 10;    


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKey(KeyCode.A))
        {
           
            //fuerza a la izq
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(-vel, this.GetComponent<Rigidbody2D>().velocity.y);
        } else if (Input.GetKey(KeyCode.D))
        {
            // fuerza a la derecha
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(vel, this.GetComponent<Rigidbody2D>().velocity.y);
        } else
        {
            // PARA
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, this.GetComponent<Rigidbody2D>().velocity.y);
        }
        if(Input.GetKeyDown(KeyCode.S))
        {
            // SALTA
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, jumpForce));
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            // DISPARA
            //Panyum();
            PanyumPool();
        }
    }
    /**
     * FUNCI�N DE DISPARAR CON POOL
     */
    private void PanyumPool()
    {
        print(this.transform.childCount);
        for (int i =0; i<this.transform.childCount; i++)
        {
            GameObject cosita = this.transform.GetChild(i).gameObject;
            print(cosita.name);
            if (!cosita.activeInHierarchy)
            {
                cosita.SetActive(true);
                cosita.transform.position = this.transform.position;
                cosita.GetComponent<Rigidbody2D>().velocity = new Vector2(-cosita.GetComponent<BalaKnuckles>().spd, 0);
                break;
            }
        }

    }

    /**
     * FUNCI�N DE DISPARO NORMAL DE TODA LA VIDA DESACTIVADA
     */
    //private void Panyum()
    //{
    //    GameObject bang = Instantiate(bala);

    //    //LA INSTANCIAMOS EN EL MISMO SITIO
    //    //bang.transform.position = this.transform.position;

    //    //LA POSICI�N LA AVANZAMOS UN POCO
    //    bang.transform.position = new Vector3(this.transform.position.x - 1f, this.transform.position.y, 0);
    //}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == "Malo")
        {
            print("El enemigo contra el que he colisionado ten�a un hp de: " + collision.GetComponent<Caca>().so.hp);
            Destroy(collision.gameObject);
            vidas--;
        }
    }

    /* ALGUNOS APUNTES */
    /* ESTA FUNCI�N NO HACE NADA, SON APUNTES B�SICOS */
    public void RepasoDeFuerzasEnUnity()
    {
        /* --- RIGIDBODY2D --- */
        // se aplica velocidad de 5 (hacia la derecha) en X, 0 en Y
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(5, 0);
        // se aplica velocidad de 5 (hacia la derecha) en X y la misma que ya tenga en Y
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(5, this.GetComponent<Rigidbody2D>().velocity.y);
        // a�adir fuerza al salto, debemos aplicar fuerza en Y
        this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 500));

        /* --- TRANSFORM --- */
        // para establecer la posici�n de un objeto creado por otro 
        // aqu� le damos la misma posici�n del objeto que la crea
        bala.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, 0);
        // aqu� le damos una posici�n en Y aleatoria y la X del que la crea
        bala.transform.position = new Vector3(this.transform.position.x, Random.Range(-2f,6f), 0);





    }
}
