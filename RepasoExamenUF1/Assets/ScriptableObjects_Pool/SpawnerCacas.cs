using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEditor.PlayerSettings;

public class SpawnerCacas : MonoBehaviour
{
    public GameObject caca;
    public List<SOCaca> listaCacas;


    void Start()
    {
        StartCoroutine(SpawnCacas());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

   public IEnumerator SpawnCacas()
    {
        while(true)
        {
            //int randomIndex = Random.Range(0, listaCacas.Count);
            //SOCaca selectedCaca = listaCacas[randomIndex];

            // Instanciar el enemigo
            GameObject nuevaCaca = Instantiate(caca);
            //Caca cacaComponent = enemyCaca.GetComponent<Caca>();
            nuevaCaca.transform.position = this.transform.position;
            nuevaCaca.GetComponent<Caca>().so = listaCacas[Random.Range(0, listaCacas.Count)];
            nuevaCaca.GetComponent<Caca>().ActivarCaca();

            yield return new WaitForSeconds(5);
        }
    }
}
