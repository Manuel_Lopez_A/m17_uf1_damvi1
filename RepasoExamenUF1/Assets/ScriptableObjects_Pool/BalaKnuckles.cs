using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BalaKnuckles : MonoBehaviour
{
    public int spd = 6;
    private Rigidbody2D rb;

    void Start()
    {
        //  LE DAMOS VELOCIDAD AL CREARLA EN EL POOL
        rb = this.GetComponent<Rigidbody2D>();
        //rb.velocity = new Vector2(-spd, 0);
    }

    // Update is called once per frame
    void Update()
    {

        // SI SALE DE LA PANTALLA LO ELIMINAS
        if (rb.transform.position.x < -11)
        {
            //Destroy(this.gameObject);  // si tenemos la funci�n "panyum" sin pool
            this.gameObject.SetActive(false);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        print("bala ha impactado contra " + collision.name);

        if (collision.tag == "Malo")
        {
            Destroy(collision.gameObject);
          //  Destroy(this.gameObject);  // si tenemos la funci�n "panyum" sin pool
          this.gameObject.SetActive(false);
        }

    }
}
