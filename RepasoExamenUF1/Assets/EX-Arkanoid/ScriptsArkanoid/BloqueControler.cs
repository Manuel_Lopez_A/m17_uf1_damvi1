using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BloqueControler : MonoBehaviour
{

    //meter una referencia a un powerUP
    
    public GameObject powerUp;
    public SOPowerUp[] powerUps;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "Bola")
        {
            DestruccionBloque();
        }
    }
    public void DestruccionBloque()
    {
        int powerUpN = Random.Range(0, 2);
        if (powerUpN == 0)
        {
            //instanciar el powerUp
            GameObject nuevoPowerUp = Instantiate(powerUp);
            nuevoPowerUp.transform.position = this.transform.position;
            nuevoPowerUp.GetComponent<PowerUp>().so = powerUps[Random.Range(0, powerUps.Count())];
            nuevoPowerUp.GetComponent<PowerUp>().ActivarPowerUp();
       
        }
        Destroy(gameObject);
    }
}
