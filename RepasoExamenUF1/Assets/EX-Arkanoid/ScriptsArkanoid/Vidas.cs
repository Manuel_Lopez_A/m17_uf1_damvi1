using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vidas : MonoBehaviour
{
    public int vidas = 3;
    void Start()
    {
        GameObject.Find("Bola").GetComponent<BolaControler>().onCaida += RestarVidas;
        this.gameObject.GetComponent<TMPro.TextMeshProUGUI>().text = "Vidas: " + vidas;

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void RestarVidas()
    {
        vidas--;
        this.gameObject.GetComponent<TMPro.TextMeshProUGUI>().text = "Vidas: " + vidas;
    }
}
