using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BolaControler : MonoBehaviour
{
    public int spd = 5;

    public delegate void caida();
    public event caida onCaida;

    void Start()
    {
        //this.transform.eulerAngles = new Vector3(0, 0, Random.Range(90f, 270f));
        // no quiero que caiga tan de lado
        this.transform.eulerAngles = new Vector3(0, 0, Random.Range(110f, 250f));
        this.GetComponent<Rigidbody2D>().velocity = this.transform.up * spd;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Gol")
        {
            ReanudarBola();
            onCaida?.Invoke();
        }
    }

    private void ReanudarBola()
    {
        this.transform.position = new Vector3(0, 0, 0);
        this.transform.eulerAngles = new Vector3(0, 0, Random.Range(110f, 250f));
        this.GetComponent<Rigidbody2D>().velocity = this.transform.up * spd;
    }





}
