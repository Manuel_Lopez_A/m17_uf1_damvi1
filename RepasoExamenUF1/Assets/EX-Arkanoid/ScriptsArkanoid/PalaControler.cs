using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PalaControler : MonoBehaviour
{

    public int vidas = 3;
    public float vel = 10f;

    public GameObject bola;
    public BolaControler bolaControler;

    // Start is called before the first frame update
    void Start()
    {
        //bola.GetComponent<BolaControler>().onCaida += ReiniciarPala;
        bolaControler.onCaida += ReiniciarPala;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.A))
        {
            //fuerza a la izq
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(-vel, this.GetComponent<Rigidbody2D>().velocity.y);
        }
        else if (Input.GetKey(KeyCode.D))
        {
            // fuerza a la derecha
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(vel, this.GetComponent<Rigidbody2D>().velocity.y);
        }
        else
        {
            this.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "PowerUp")
        {
            print("El color del powerUP es" +collision.GetComponent<PowerUp>().so.color);
            Destroy(collision.gameObject);

        }
    }

    public void ReiniciarPala()
    {
        vidas--;
        print(vidas);
        this.gameObject.transform.position = new Vector3(0, -3, 0);
        if (vidas == 0)
        {
            print(vidas);
            Destroy(this.gameObject);
            SceneManager.LoadScene("IniciArakanoid");
        }
    }
}
