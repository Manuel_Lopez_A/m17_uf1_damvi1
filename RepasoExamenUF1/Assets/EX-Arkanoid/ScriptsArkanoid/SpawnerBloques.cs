using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerBloques : MonoBehaviour
{
    public GameObject bloque;

    private void Start()
    {
        StartCoroutine(Spawn());
    }


    IEnumerator Spawn()
    {
        while (true)
        {

            float x = Random.Range(-13f, 13f);
            float y = Random.Range(-7f, 7f);
            if (true/*si no choca con bloques */)
            {
                GameObject newBloque = Instantiate(bloque);

                yield return new WaitForSeconds(2);
            }
        }
    }
}
