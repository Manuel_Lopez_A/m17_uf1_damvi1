using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerRepaso : MonoBehaviour
{
    public GameObject [] enemigos;
    
    void Start()
    {
        StartCoroutine(CrearEnemigo());
    }


    void Update()
    {
        
    }

    IEnumerator CrearEnemigo()
    {
       while (true)
        {
            GameObject newEnemigo = Instantiate(enemigos[Random.Range(0,2)]);
            newEnemigo.transform.position = new Vector3(this.transform.position.x, Random.Range(-2f, 4f), 0);
            
       
            
            
            yield return new WaitForSeconds(Random.Range(2, 5));
        }
        
    }
}
