using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BatController : MonoBehaviour
{

    // [SerializeField] private float spd = 3f;
    public int spd = 3;

    void Start()
    {
        this.GetComponent<Rigidbody2D>().velocity = new Vector2 (-spd, 0);
    }

    
    void Update()
    {
        
    }
    public void ActualizarVel()
    {
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(-spd, 0);
        this.GetComponent<AudioSource>().Play();
        print("actualizada velocidad del BAT");
    }
}
