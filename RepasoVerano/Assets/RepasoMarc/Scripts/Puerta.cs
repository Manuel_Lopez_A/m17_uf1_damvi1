using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puerta : MonoBehaviour
{
    public RigbyController rigby;

    void Start()
    {
        rigby.onRigby += DesactivarPuerta;
    }

    public void DesactivarPuerta()
    {
        this.transform.tag = "lila";
        this.GetComponent<SpriteRenderer>().color = Color.green;

        StartCoroutine(ReactivarPuertaDespuesDeEspera());
    }

    private IEnumerator ReactivarPuertaDespuesDeEspera()
    {
        yield return new WaitForSeconds(3f);

        this.transform.tag = "Finish";
        this.GetComponent<SpriteRenderer>().color = Color.red;
    }

}
