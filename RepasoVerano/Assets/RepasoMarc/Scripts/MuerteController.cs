using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MuerteController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(-1, 0);
    }

    // Update is called once per frame
    void Update()
    {
        if (this.gameObject.transform.position.x < -10) 
        {
            Destroy(this.gameObject); 
        }
    }
}
