using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RosaController : MonoBehaviour
{
    [SerializeField] int spd;


    public delegate void Punto(int p);
    public event Punto OnPunto;  // este es el evento



    void Start()
    {
        this.GetComponent<Rigidbody2D>().velocity = new Vector2 (spd, 0);
        this.GetComponent<AudioSource>().Play();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        print("Bala ha chocado contra " + collision.name);

        if (collision.gameObject.CompareTag("Muerte"))
        { 
            Destroy(collision.gameObject);

            OnPunto?.Invoke(2); // if (OnPunto != null){ Invoke() }

            Destroy(this.gameObject);
        }
        else if (collision.gameObject.CompareTag("Malisimo"))
        {
            collision.GetComponent<BatController>().spd *= 2;
            collision.GetComponent<BatController>().ActualizarVel();
            collision.GetComponent<SpriteRenderer>().color = Color.red;

            OnPunto?.Invoke(5); // if (OnPunto != null){ Invoke() }

            Destroy(this.gameObject);
        }
    }
}
