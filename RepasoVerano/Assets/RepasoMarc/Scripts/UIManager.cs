using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    int puntos = 0;

    public void HazAlgo(int p)
    {
        puntos += p;
        this.GetComponent<TMPro.TextMeshProUGUI>().text = "Puntos: " + puntos;
    }
}
