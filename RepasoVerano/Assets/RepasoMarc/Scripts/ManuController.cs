using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ManuController : MonoBehaviour
{
    Rigidbody2D rigidbodyManu;
    public float spd;
    public GameObject rosa;
    private bool cooldown;
    public UIManager manager;

    void Start()
    {
        cooldown = false;
        spd = 5f;
        rigidbodyManu = this.GetComponent<Rigidbody2D>();        
    }

    void Update()
    {
        // se le aplica una velocidad constante que parar� cuando se deje de pulsar el bot�n
        if (Input.GetKey(KeyCode.A))
        {
            rigidbodyManu.velocity = new Vector2(-spd, rigidbodyManu.velocity.y);
        }
        else if (Input.GetKey(KeyCode.D))
        {
            rigidbodyManu.velocity = new Vector2(spd, rigidbodyManu.velocity.y);
        } 
        else
        {
            rigidbodyManu.velocity = new Vector2(0, rigidbodyManu.velocity.y);
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            rigidbodyManu.AddForce(new Vector2(0, 500));
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            if(!cooldown)
            {
                GameObject newRosa = Instantiate(rosa);
                newRosa.transform.position = rigidbodyManu.transform.position;
                newRosa.GetComponent<RosaController>().OnPunto += manager.HazAlgo;
                cooldown = true;
                StartCoroutine(Cooldown());            
            }
        }
    }

    IEnumerator Cooldown()
    {
        // no hace falta un bucle while(true) porque solo lo hace una vez y fuera.
            yield return new WaitForSeconds(1);
            cooldown = false;        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        print("Manu ha chocado contra "+ collision.name);

        if (collision.gameObject.CompareTag("Muerte"))
        {
            SceneManager.LoadScene("GameOverScene");
        } else if (collision.gameObject.CompareTag("Malisimo"))
        {
            SceneManager.LoadScene("GameOverScene");
        }
    }
}
