using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RigbyController : MonoBehaviour
{
    Rigidbody2D rigidbodyRigby;
    public float spd;

    //DELEGADO:
    public delegate void RigbyDelegate(); //
    public RigbyDelegate onRigby;

    void Start()
    {
        rigidbodyRigby = this.GetComponent<Rigidbody2D>();
        spd = 5;
    }


    void Update()
    {
        if (Input.GetKey(KeyCode.A))
        {
            rigidbodyRigby.velocity = new Vector2(-spd, rigidbodyRigby.velocity.y);
        }
        else if (Input.GetKey(KeyCode.D))
        {
            rigidbodyRigby.velocity = new Vector2(spd, rigidbodyRigby.velocity.y);
        }
        else
        {
            rigidbodyRigby.velocity = new Vector2(0, rigidbodyRigby.velocity.y);
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            rigidbodyRigby.AddForce(new Vector2(0, 500));
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        // si la puerta est� roja, MATA
        if (collision.CompareTag("Finish"))
        {
            Destroy(this.gameObject);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "rosa")
        {
            // invoca las funciones suscritas a "onRigby"
            onRigby?.Invoke();
            /*
            if (onRigby != null)
            {
                onRigby();
            }
            */
        }
    }
}
