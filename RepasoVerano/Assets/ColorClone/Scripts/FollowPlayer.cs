using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
    [SerializeField] private Transform playerTransform;

    void Update()
    {
       if (playerTransform.position.y > this.transform.position.y)
        {
            this.transform.position = new Vector3(transform.position.x, playerTransform.position.y, transform.position.z); 

        }
    }
}
