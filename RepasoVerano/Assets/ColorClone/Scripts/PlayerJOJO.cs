using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerJOJO : MonoBehaviour
{
    public float verticalForce = 400f;
    Rigidbody2D playerRb;
    SpriteRenderer playerSR;

    public float restartDelay = 1f;
    [SerializeField] private ParticleSystem playerParticles;
    [SerializeField] private Color orangeColor;
    [SerializeField] private Color violetColor;
    [SerializeField] private Color pinkColor;
    [SerializeField] private Color cyanColor;
    // playerSR.color = Color.yellow;

    private string currentColor;


    void Start()
    {
        playerRb = GetComponent<Rigidbody2D>();
        playerSR = GetComponent<SpriteRenderer>();

        ChangeColor();

    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space)) {

            playerRb.velocity = Vector2.zero; // es igual que decir new Vector2(0,0)
            playerRb.AddForce(new Vector2(0, verticalForce));
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        print("Colision con: "+ collision.gameObject.name);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        // LLEGADA
        if (collision.CompareTag("llegada"))
        {
            Instantiate(playerParticles, transform.position, Quaternion.identity);
            gameObject.SetActive(false);
            Invoke("LoadNextScene", restartDelay);            
            return;
        }

        // CHANGE COLOR
        // if (collision.gameObject.CompareTag("colorChanger"))
        if (collision.CompareTag("colorChanger"))
        {
            ChangeColor();
            Destroy(collision.gameObject);
            return;
        }


        // MUERTE
        if ( collision.gameObject.tag != this.currentColor)
        {
            // llamamos al efecto de particulas, quaternion-identity es que no rote ni nada
            Instantiate(playerParticles, transform.position, Quaternion.identity);
            gameObject.SetActive(false);
            //RestartScene();
            // queremos que haya un delay antes de restaurar escena
            Invoke("RestartScene", restartDelay);
        }
    }
    void LoadNextScene()
    {
        int activeSceneIndex = SceneManager.GetActiveScene().buildIndex; // TO DO => puedo quitar esta linea?
        SceneManager.LoadScene(activeSceneIndex + 1 );

    }
    void RestartScene()
    {
        int activeSceneIndex = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(activeSceneIndex);

        // SceneManager.LoadScene("Principiantes"); // CARGA ESCENA POR SU NOMBRE
        // SceneManager.LoadScene(SceneManager.GetActiveScene().name); // CARGA ESCENA ACTIVA
    }
    void ChangeColor()
    {
        int randomNumber = Random.Range(0, 4);

        if (randomNumber == 0)
        {
            playerSR.color = orangeColor;
            currentColor = "amarillo";
        } else if (randomNumber == 1)
        {
            playerSR.color = violetColor;
            currentColor = "lila";
        } else if (randomNumber == 2)
        {
            playerSR.color = pinkColor;
            currentColor = "rosa";
        } else if (randomNumber == 3)
        {
            playerSR.color = cyanColor;
            currentColor = "cyan";
        }
    }
}
