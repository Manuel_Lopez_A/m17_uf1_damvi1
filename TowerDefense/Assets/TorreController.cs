using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TorreController : MonoBehaviour
{
    public int hpTorre = 100;
 
    public void RecibirDanyo(int i)
    {
        hpTorre -= i;
        print("torre tiene HP:" + hpTorre);

        StartCoroutine(CambiarColorTemporizador(0.5f));

        /* si pierdes vida game Over */
        if (hpTorre <= 0) 
        {
            SceneManager.LoadScene("GameOver");
        }
    }
    IEnumerator CambiarColorTemporizador(float duracion)
    {
        Renderer TORRE = GetComponent<Renderer>();
        Color originalColor = Color.white;

        TORRE.material.color = Color.red;

        yield return new WaitForSeconds(duracion);

        TORRE.material.color = originalColor;
    }
    public void Cura(int i)
    {
        hpTorre += i;
        print("torre tiene HP:" + hpTorre);

        StartCoroutine(CambiarColorTemporizador(0.5f));

        /* si pierdes vida game Over */
        if (hpTorre <= 0)
        {
            SceneManager.LoadScene("GameOver");
        }
    }
}
