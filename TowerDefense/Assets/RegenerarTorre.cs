using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RegenerarTorre : MonoBehaviour
{
   public ContadorPuntos puntos;
   public TorreController torreController;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
  
    }


   public void RegenerarVida()
   {
        if (puntos.Puntos >= 200) 
        {
            puntos.Puntos -= 200;
            torreController.Cura(10);
        }
   }
}

