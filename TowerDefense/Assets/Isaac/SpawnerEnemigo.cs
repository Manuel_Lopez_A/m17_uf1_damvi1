using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerEnemigo : MonoBehaviour
{
    public List<GameObject> enemigos;

    void Start()
    {
        StartCoroutine(GenerarEnemigo());
    }

    IEnumerator GenerarEnemigo()
    {
        while (true)
        {
            print("Nuevo Enemigo creado");
            // Seleccionar un enemigo aleatorio de la lista
            int indiceAleatorio = Random.Range(0, enemigos.Count);
            GameObject enemigoSeleccionado = enemigos[indiceAleatorio];

            Instantiate(enemigoSeleccionado);
            //Vector2 pos = new Vector2(this.gameObject.transform.position.x, UnityEngine.Random.Range(2f, -4f));
            enemigoSeleccionado.GetComponent<Transform>().position = this.gameObject.transform.position;
            enemigoSeleccionado.GetComponent<Rigidbody2D>().velocity = Vector2.left * 5;
            yield return new WaitForSeconds(2.0f);
        }
    }

}
