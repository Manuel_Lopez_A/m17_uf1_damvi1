using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class EnemigosSO : ScriptableObject
{

    public GameObject enemyPrefab;
    public int hp;
    public int atk;
    public int nivel;
   // public Sprite img;
}
