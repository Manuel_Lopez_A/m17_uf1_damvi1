using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(fileName = "ContadorMonera")]
public class ContadorMonera : ScriptableObject
{
    public int ContadorMoneras = 0;

    void Start() {
        ContadorMoneras = 0;
    }
    public void Moneras()
    {
        ContadorMoneras++;
    }
}
