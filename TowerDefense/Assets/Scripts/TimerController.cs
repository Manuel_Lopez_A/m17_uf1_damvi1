using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TimerController : MonoBehaviour
{
    public float totalTime = 180f; 
    public TextMeshProUGUI timerText; 

    private float timeRemaining;

    private void Start()
    {
        timeRemaining = totalTime;
    }

    private void Update()
    {
        timeRemaining -= Time.deltaTime;

        // Actualizar el texto del temporizador con el formato MM:SS
        int minutes = Mathf.FloorToInt(timeRemaining / 60); // lo que queda de tiempo lo dividimos en 60 minutos
        int seconds = Mathf.FloorToInt(timeRemaining % 60); // el residuo son los segundos...
        string timeText = string.Format("{0:00}:{1:00}", minutes, seconds);
        timerText.text = timeText;

        // Cambiar el color del texto seg�n el tiempo restante
        if (timeRemaining <= 60f) 
        {
            timerText.color = Color.yellow;
        }

        if (timeRemaining <= 20f) 
        {
            timerText.color = Color.red;
        }

        if (timeRemaining <= 0f)
        {
            SceneManager.LoadScene("GameOver");
        }
    }
}