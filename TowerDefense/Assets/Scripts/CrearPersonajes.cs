using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrearPersonajes : MonoBehaviour
{
    public List<GameObject> ListaPersonaje;
    public GameObject personajes;
    public ContadorMonera Moneras;
    public MostraPrecio Precio;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void pagar()
    {
        Moneras.ContadorMoneras -= Precio.Precio;
    }
    public void Pulsa()
    {
        if (Moneras.ContadorMoneras>=Precio.Precio)
        {
            pagar();
            GameObject go = Instantiate(ListaPersonaje[0]);
            go.transform.position = personajes.transform.position;
        }
        
    }
}
