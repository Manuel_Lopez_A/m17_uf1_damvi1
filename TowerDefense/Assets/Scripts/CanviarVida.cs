using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanviarVida : MonoBehaviour
{
    public Sprite spriteRojo;
    public Sprite spriteNaranja;
    public Sprite spriteAmarillo;
    public Sprite spriteVerde;
    public Sprite spriteVacio;
    private TorreController torreController;
    
     public GameObject torreGameObject;

    private SpriteRenderer spriteRenderer;

    // Start is called before the first frame update
    void Start()
    {
        // Obtener el componente SpriteRenderer del objeto
        spriteRenderer = GetComponent<SpriteRenderer>();
        
        torreController = torreGameObject.GetComponent<TorreController>();
        
        // Obtener el componente TorreController del objeto o de uno de sus hijos
       // torreController = GetComponentInChildren<TorreController>();
       
    }

    // Update is called once per frame
    void Update()
    {
        int vidaTorre = torreController.hpTorre;

        if (vidaTorre != null)
        {
            if (vidaTorre == 100)
            {
                spriteRenderer.sprite = spriteVerde;
            }

            if (vidaTorre <= 80)
            {
                spriteRenderer.sprite = spriteAmarillo;
            }

            if (vidaTorre <= 50)
            {
                spriteRenderer.sprite = spriteNaranja;
            }

            if (vidaTorre <= 25)
            {
                spriteRenderer.sprite = spriteRojo;
            }

            if (vidaTorre <= 0)
            {
                spriteRenderer.sprite = spriteVacio;
            }
        }
    }
}

