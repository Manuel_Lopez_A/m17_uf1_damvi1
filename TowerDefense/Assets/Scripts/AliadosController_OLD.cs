using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting.Antlr3.Runtime.Tree;
using UnityEngine;


// *************************************************************************************************
// ESTE SCRIPT ES UNA PRUEBA DE MOVIMIENTOS CON RAT�N QUE NO ME FUNCIONA, NO LA BORRO POR SI ACASO..
// *************************************************************************************************


public class AliadosController_OLD : MonoBehaviour
{

    //public GameObject aliadoPrefab;
    //public Aliado[] definiciones;


    private GameObject selectedObject; // Almacena el objeto seleccionado
    private bool isMoving = false; // Indica si el objeto debe moverse
    private Vector3 targetPosition; // La posici�n a la que el objeto debe moverse
    
    
    public float moveSpeed = 5f; // Velocidad de movimiento del objeto
    public float force = 0f;
    public int hpActual = 0;
    public int hpMax = 0;


    void Update()
    {
        if (Input.GetMouseButtonDown(0)) // Comprueba si se hizo clic con el bot�n izquierdo del rat�n
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                if (hit.collider.gameObject.CompareTag("Selectable")) // Comprueba si el objeto clicado es seleccionable
                {
                    selectedObject = hit.collider.gameObject; // Almacena el objeto seleccionado
                }
                else
                {
                    targetPosition = hit.point; // Almacena la posici�n en la que se hizo clic
                    isMoving = true; // Inicia el movimiento del objeto
                }
            }
        }

        if (isMoving)
        {
            MoveObject(); // Llama al m�todo para mover el objeto
        }
    }

    void MoveObject()
    {
        selectedObject.transform.position = Vector3.MoveTowards(selectedObject.transform.position, targetPosition, moveSpeed * Time.deltaTime);

        if (selectedObject.transform.position == targetPosition)
        {
            isMoving = false; // Detiene el movimiento cuando el objeto alcanza el objetivo
        }
    }
}
