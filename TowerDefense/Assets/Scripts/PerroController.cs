using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PerroController : MonoBehaviour
{
    
    public float vel;
    private Rigidbody2D rb;
    //public AudioClip sonidoMoneda; // extra sonido
    private SpriteRenderer spriteRenderer;

    void Start()
    {
        rb = this.GetComponent<Rigidbody2D>();
        // audioSource = this.GetComponent<AudioSource>();
        spriteRenderer = this.GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        PerroMovimiento();
    }

    public void PerroMovimiento()
    {
        {
            // Obtener las entradas del teclado
            float moveX = Input.GetAxisRaw("Horizontal");
            float moveY = Input.GetAxisRaw("Vertical");

            // Calcular la dirección del movimiento
            Vector2 movement = new Vector2(moveX, moveY).normalized;

            // Mover el personaje
            rb.velocity = movement * vel;

            girarImagen( moveX, moveY) ;
        }
    }

    private void girarImagen(float moveX , float moveY)
    {
        if (moveX < 0)
        {
            spriteRenderer.flipX = true; // Invertir el sprite horizontalmente
        }
        else if (moveX > 0)
        {
            spriteRenderer.flipX = false; // Restaurar la orientación normal del sprite
        }
    }

    //public void OnTriggerEnter2D(Collider2D collision)
    //{
    //    print("iker " + collision.tag);
    //    if (collision.tag == "Moneda")
    //    {
    //        PerroMoneda();
    //    }
    //}

    //public void PerroMoneda()
    //{
    //    print("cositas");
    //    this.GetComponent<AudioSource>().Play(); // extra sonido
    //    this.GetComponent<AudioSource>().PlayOneShot(sonidoMoneda); // extra sonido
    //}

}
