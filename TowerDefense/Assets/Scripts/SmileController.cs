using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEditor.PackageManager;
using UnityEngine;

public class SmileController : MonoBehaviour
{
    
    [SerializeField] List<Transform> wayPoints;
    float velocidad = 2;
    float distanciaCambio = 0.2f;
    byte siguientePosicion = 0;
    private List<Transform> waypoints;

    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.MoveTowards(
            transform.position,
            wayPoints[siguientePosicion].transform.position,
            velocidad * Time.deltaTime); //Tiempo transcurrido en ese fotograma multiplicado por la velocidad
        
        if (Vector3.Distance(transform.position,
            wayPoints[siguientePosicion].transform.position) < distanciaCambio)
        {
            siguientePosicion++;
            if (siguientePosicion >= wayPoints.Count)
                Destroy(gameObject);
        }
    }

    public void recibirDanyo()
    {
        Destroy(gameObject);
    }


    //private void OnCollisionEnter2D(Collision2D collision)
    //{
    //    if (collision.transform.tag == "Torre")
    //    {
    //        print("TORREEEE");
    //        Destroy(gameObject);
    //    }
    //}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == "Torre")
        { 
            /* ES UNA GUARRADA PERO ES IMPOSIBLE ACCEDER A SU FUERZA DE ATAQUE TAL Y COMO LO TENEMOS CON SCRIPTABLE OBJECTS...*/
           int fuerza = 0;

           if (this.GetComponent<Transform>().tag == "slime1")
            {
                fuerza = 1;
            } 
            else if (this.GetComponent<Transform>().tag == "slime2")
            {
                fuerza = 2;
            }
            else
            {
                fuerza = 3;
            }
            collision.GetComponent<TorreController>().RecibirDanyo(fuerza *2);
            Destroy(gameObject);
            
        }


    }

}
