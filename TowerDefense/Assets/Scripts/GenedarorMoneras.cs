using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Pool;

public class GenedarorMoneras : MonoBehaviour
{
    public List<GameObject> ObjeCrea;
    public int TEspera;
    //[SerializeField] private GameObject MoneraPrefab;
    //[SerializeField] private int poolSize = 10;
    //[SerializeField] private List<GameObject> MoneraLista;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Genedar());
    }

    IEnumerator Genedar()
    {
        while (true)
        {
            float l = Random.Range(-8.5f, 8.5f);
            float a = Random.Range(-4f, 4f);
            if (!(l <= 5.3 && l >= 6.4 && a <= 2.9 && a >= 3.3|| l <= 4.8 && l >= 6.4 && a <= -3.5 && a >= -4))
            {
                GameObject go = Instantiate(ObjeCrea[0]);
                go.transform.position = new Vector2(l, a);
                yield return new WaitForSeconds(TEspera);
            }
            Debug.Log("lognitut: " + a + ", altura: " + l);
        }
    }
}
