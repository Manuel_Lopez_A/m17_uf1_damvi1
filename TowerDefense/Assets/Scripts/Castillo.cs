using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Castillo : MonoBehaviour
{
    public int vida;
    private float vidaActual;
    public float VidaActual { get => vidaActual; set => vidaActual = value; }
    public int punto = 0;
    public int monera = 0;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Alpha0))
        {
            VidaActual = 0;
        }
        if (Input.GetKey(KeyCode.Alpha1)) 
        {
            VidaActual = (float)(vida * 0.25);
        }
        if (Input.GetKey(KeyCode.Alpha2)) 
        {
            VidaActual = (float)(vida * 0.5);
        }
        if (Input.GetKey(KeyCode.Alpha3)) 
        {
            VidaActual = (float)(vida * 0.75);
        }
        if (Input.GetKey(KeyCode.Alpha4)) 
        {
            VidaActual = vida;
        }
        else 
        {
            punto = punto + 10;
        }
    }
}
