using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class CoinController : MonoBehaviour
{
   public AudioClip recogerMonedas;
   public ContadorMonera CM;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag== "Jugador")
        {
            Invoke("Moneras", 0f);
            print("moneda ha chocado contra" + collision.name);
            //LLAMAMOS AL SR. SINGLETON 

            ControladorSonido.Instance.EjecutarSonido(recogerMonedas); 
            Destroy(this.gameObject);
        }
    }
    private void Moneras()
    {
        CM.Moneras();
    }
}
