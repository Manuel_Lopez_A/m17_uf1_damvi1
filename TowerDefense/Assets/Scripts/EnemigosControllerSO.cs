using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigosControllerSO : MonoBehaviour
{
    public EnemigosSO[] enemigosPrefabs;
    public Transform[] spawnerPuntos;
    public float tiempoGenerarEnemigos = 3f;

    void Start()
    {
        StartCoroutine(GenerarEnemigo());
        
    }

    IEnumerator GenerarEnemigo()
    {
         while (true)
         {
            print("longitud lista1: "+enemigosPrefabs.Length);

             // Seleccionar un enemigo aleatorio de la lista
             int indiceAleatorio = Random.Range(0, enemigosPrefabs.Length);
             EnemigosSO enemigoSeleccionado = enemigosPrefabs[indiceAleatorio];
            GameObject enemigoPrefab = enemigoSeleccionado.enemyPrefab;

             Instantiate(enemigoPrefab);
             Vector2 pos = new Vector2(transform.position.x, Random.Range(2f, -4f));
             enemigoPrefab.transform.position = transform.position;
             enemigoPrefab.GetComponent<Rigidbody2D>().velocity = Vector2.left * 5;
             yield return new WaitForSeconds(tiempoGenerarEnemigos);
            print("longitud lista2: " + enemigosPrefabs.Length);
        }
        
    }


    // public void SpawnRandomEnemigo()
    // {
    //     int randomIndex = Random.Range(0, enemigosPrefabs.Length);
    //     int randomSpawnPoint = Random.Range(0, spawnerPuntos.Length);

    //     EnemigosSO enemyData = enemigosPrefabs[randomIndex];
    //     Transform spawnerPunto = spawnerPuntos[randomSpawnPoint];

    //     GameObject enemyInstance = Instantiate(enemyData.enemyPrefab, spawnerPunto.position, Quaternion.identity);

    //     Vector2 pos = new Vector2(this.gameObject.transform.position.x, UnityEngine.Random.Range(2f, -4f));
    //     enemyInstance.GetComponent<Transform>().position = this.gameObject.transform.position;
    //     enemyInstance.GetComponent<Rigidbody2D>().velocity = Vector2.left * 5;

    // }

}
