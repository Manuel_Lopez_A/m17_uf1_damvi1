using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// *************************************************************************************************
// SCRIPT DE MOVIMIENTO POR MOUSE QUE S� FUNCIONA !!!!!!!!!
// *************************************************************************************************

public class MouseController : MonoBehaviour
{
    public float vel;
   
    private Vector3 obj;
    public static List<MouseController> AliadosSeleccionables = new List<MouseController>();
    private bool seleccionado = false;



    void Start()
    {
        
        AliadosSeleccionables.Add(this);
        obj = this.transform.position;
    }

    // Update is called once per frame
    void Update()
    {

        // click izquierdo && seleccionado =>
        if(Input.GetMouseButton(0) && seleccionado)
        {
            //convierte la posici�n actual del puntero del mouse (Input.mousePosition)
            //desde coordenadas de pantalla a coordenadas del mundo en el juego
            obj = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            // Z de obj se establece en la misma Z del objeto actual, lo que garantiza que el objeto siga
            // al puntero solo en el plano Z actual
            obj.z = this.transform.position.z; 

        }
        // MoveTowards calcula la distancia entre los dos puntos y hace que la recorra de manera suave,
        // con Time.deltatime nos aseguramos velocidad independiente del procesador
        this.transform.position = Vector3.MoveTowards(this.transform.position, obj, vel * Time.deltaTime);
        

        if(seleccionado ) { this.gameObject.GetComponent<SpriteRenderer>().color = Color.blue; }
        else { this.gameObject.GetComponent<SpriteRenderer>().color = Color.white; }
    }
    private void OnMouseDown()
    {
        seleccionado=true;
        
        foreach(MouseController aliado in AliadosSeleccionables)
        {
            if (aliado != this)
            {
                aliado.seleccionado = false;
                //aliado.gameObject.GetComponent<SpriteRenderer>().color = Color.white; //quiero quitarle el color
            }
        }
    }

    private void OnMouseOver()
    {
        
    }
}
