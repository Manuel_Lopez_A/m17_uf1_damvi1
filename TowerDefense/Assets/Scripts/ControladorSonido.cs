using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControladorSonido : MonoBehaviour
{
    public static ControladorSonido Instance;
    private AudioSource audioSource;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        } else
        {
            Destroy(gameObject);
        }
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    public void EjecutarSonido(AudioClip sonido)
        {
        print("ha entrado en EjecutarSonido()");

            audioSource.PlayOneShot(sonido);
        
        print("deber�a sonar...");
        }
    }
